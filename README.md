# FindInvest
![alt text](https://gitlab.com/Tom-michel/FindInvest/-/raw/tom/static/img/logo1.png?raw=true)


Plateforme de recherche d'investissement pour les étudiants porteurs de projets de l'université de yaoundé 1.

Equipe de développement
----------------------- 

    noms-matricule-github:

	- BIKIM BI NSOGA JEAN PETIT YVELOS  19M2596 (@jean.bikim)
	- BTOMPE TCHEUFFA MICHEL RUFIN     19M2535 (@Tom-michel)
	- MENGUE OWONA VICTOR AUDREY	  19M2241 (@audrey.mengue)
	- NDEMAFO NKENANG FLAVIE DAVILA  19M2267 (@flaviedaniela)
	- NGUETE TCHATAT DAVE NEPHTALI	  19M2293 (@Mcdavens4)
	
Encadreur:
----------

	Dr. Valery MONTHE

Travail réalisé dans le cadre des UEs:
	- INF3196 : Projet I 
	- INF3176 : Environnement de développement avancé



PROFIL MOA:
-----------

	Tout entrepreneur investisseur, Ingénieur, etc.. souhaite augmenter ses capitaux en misant sur des projets prométeurs qui 
	peuvent lui rapporter un bon bénéfice. De part leur créativité et leur imagination, les étudiants semblent être la meilleure 
	cible faire gagner beaucoup d'argent décidant décidant de concrétiser certain de leurs projets en y fournissant les fonds (investir) 
	pour la mise en production.
	Il est donc nécessaire de disposer d'une plateforme, où les etudiants peuvent venir y exposer leurs idées de projets aux 
	yeux de potentiels investisseurs.



MODULE 1 : GESTION DES UTILISATEURS
-----------------------------------


Ce module conerne la gestion des utilisateur (visteurs, étudiants, investisseurs), c'est-à-dire tous les
services agissant sur les profils utilisateurs.

Il permet aux viteurs d'effectuer les tâches suivantes :
	- S'inscrire : créer un compte etudiant ou investisseur;
	- Se connecter : nécessaire pour éffectuer certaines action (publier, commenter, liker, etc.. un projet);
	- Consulter son profil : afin de véfier ses informations;
	- Modifier son profil : mettre à jour ses informations;
	- Se déconnecter : redevenir un simple visiteur.



