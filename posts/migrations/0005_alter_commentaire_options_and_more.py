# Generated by Django 4.0.4 on 2022-06-22 05:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0004_commentaire_reponse_delete_comment_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='commentaire',
            options={},
        ),
        migrations.RenameField(
            model_name='commentaire',
            old_name='auteur',
            new_name='user',
        ),
        migrations.RemoveField(
            model_name='commentaire',
            name='corps',
        ),
        migrations.RemoveField(
            model_name='commentaire',
            name='date_added',
        ),
        migrations.CreateModel(
            name='ComMessage',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('auteur', models.CharField(max_length=100)),
                ('corps', models.TextField(max_length=500)),
                ('date_added', models.DateTimeField(auto_now_add=True)),
                ('commentaire', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='contenus', to='posts.commentaire')),
            ],
            options={
                'ordering': ['-date_added'],
            },
        ),
    ]
