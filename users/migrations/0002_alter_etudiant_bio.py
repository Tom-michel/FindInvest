# Generated by Django 4.0.4 on 2022-06-23 03:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='etudiant',
            name='bio',
            field=models.TextField(blank=True, default="Je suis un étudiant de l'UNIVERSITÉ DE YAOUNDÉ 1. Je suis assez ambitieux et jai des projets que je souhaiterais mettre sur le marché.", max_length=300, null=True),
        ),
    ]
